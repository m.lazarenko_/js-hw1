// Теоретичні питання:

// 1. Оголосити змінну можна за допомогою var, const, let.

// 2. У prompt користувач вводить дані, а в confirm погоджується чи ні.

// 3. Неявне перетворення типів - це  перетворення одного типу в інше, коли js не може виконати дану операцію він намагається перетворити елемент в інший тип для виконання операції. Приклад: let a = '3' * 3;

// TASK 1

let admin;
const name = "Maksym";

admin = name;
console.log('My name is', admin);

// TASK 2

let days = Number(prompt("Enter days from 1 to 10"));
let seconds = days * 86400;
console.log(`Seconds in ${days} days: ${seconds} seconds`);


// TASK 3

let age = prompt("How old are you?");
console.log(`You are ${age} years old`);
